About
=====

This is the codified website for a Funtoo Linux development blog.

The source code includes auto-generated static web content, static site generator configurations, infrastructure automation configurations, and tools to create and manage [siris.fun](https://www.siris.fun/)

Components
==========

* Web Hosting Platform: [Cloudflare Workers Sites](https://developers.cloudflare.com/workers/platform/sites)
* Static Site Generator: [Hugo](https://gohugo.io)
* Site Theme: [hello-friend-ng Theme](https://themes.gohugo.io/themes/hugo-theme-hello-friend-ng/)

Domains
=======

* [siris.fun](https://www.siris.fun/)

Development
===========

To develop and create content for this site please setup this in your local environment:

* [Git](https://git-scm.com/downloads)
* [Golang](https://golang.org)
* [Hugo](https://gohugo.io/getting-started/installing/)
* [Wrangler](https://github.com/cloudflare/wrangler)
* A text editor like [vim](https://github.com/vim/vim) or an equivalent

Access to a terminal emulator in either macOS or Linux.

Additionally, you will have to know how to write MarkDown as all of the site content in Hugo is MarkDown formatted.

To quickly learn MarkDown use [CommonMark](https://commonmark.org).

Run the Site Locally
--------------------

Inside a terminal emulator, change into git clone directory of this repository, then execute:

```
mage run
```

This will start a live development Hugo server with the Hugo site fully built at: **http://localhost:1313/**

On Linux you can automatically open a Web Browser to the local Hugo server by executing:

```
mage open
```

Contact
=======

If you have any problems, questions, ideas, suggestions, [please contact me on Discord](https://discordapp.com/users/siris#4242)
