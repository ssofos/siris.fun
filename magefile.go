//go:build mage
// +build mage

package main

import (
	"fmt"
	"os"

	"github.com/magefile/mage/sh"
)

// build the Hugo static site locally
func Build() error {
	fmt.Println("Building siris.fun Hugo Static site...")
	return sh.RunV("hugo", "--minify", "-s", "siris")
}

var HugoLocalSite = "http://localhost:1313/"

// open a local macOS web browser to the local development Hugo web server
func Open() error {
	fmt.Println("Opening local Hugo Development Server at: %v", HugoLocalSite)
	return sh.RunV("xdg-open", "http://localhost:1313/")
}

// publish the Hugo static site to Cloudflare Workers Site using wrangler
func Publish() error {
	fmt.Println("Publishing Siris.fun site to Cloudflare Workers Site...")
	return sh.RunV(os.Getenv("HOME")+"/node_modules/.bin/wrangler", "publish")
}

// run a local development Hugo web server to locally test the Hugo static site
func Run() error {
	fmt.Println("Running Hugo local Development Server...")
	return sh.RunV("hugo", "server", "-D", "-s", "siris")
}
