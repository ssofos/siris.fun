---
title: "About"
date: 2022-07-14T13:06:16-07:00
draft: false
comments: false
images:
---

Welcome to my [Funtoo Linux](https://www.funtoo.org/) Development Blog. Join me on a journey of discovery through Funtoo Linux development. This site chronicles in technical depth the different adventures I go on with Funtoo Linux.

I am an enthusiastic Container Infrastructure Engineer, Site Reliability Engineer, Systems Engineer, Penguin Wrangler, Pythonist, Rubyist, GNU Herder, Midgardian, Westerosi, and Child of Ilúvatar.

My passion for [Funtoo Linux](https://www.funtoo.org/) runs deep as I have been using [Gentoo](https://www.gentoo.org/) based Linux distributions continuously since 2005 and then shortly thereafter switching to Funtoo Linux full time.

The expansive and world changing engineering efforts of the [Linux Kernel](https://kernel.org/) as the largest globally developed software project has and continues to provide daily inspiration to keep hacking, tinkering, and having fun.

