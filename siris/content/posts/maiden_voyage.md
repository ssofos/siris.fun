---
title: "Maiden Voyage"
date: 2022-07-14T13:12:28-07:00
draft: false
toc: false
categories:
  - General
tags:
  - Announcements
---

Welcome to Siris's Funtoo Dev Blog. This is the initial post and maiden voyage on a vast journey of discovery through Funtoo Linux Development.

In upcoming posts I will be detailing the past six months of 2022 Funtoo Linux development with some very technically exciting topics.

Thereafter this blog will be going into uncharted waters and provide lots of in engineering deep dives into areas I am actively developing on for Funtoo Linux.

Stayed tuned for more posts. This Funtoo development ship is official now sailing through the expansive ocean that is GNU/Linux distribution development!
