---
title: "Ruby"
date: 2022-07-14T13:11:53-07:00
draft: false
comments: false
categories:
  - Projects
tags:
  - Ruby
---

A [Funtoo Project](https://www.funtoo.org/Special:CargoTables/projects) that I started in 2022 is the Funtoo Ruby Project.

It is dedicated to modernizing the Ruby Programming Language on Funtoo Linux.

Check out the official project page at: **[Funtoo Ruby Project](https://www.funtoo.org/Funtoo:Ruby)**
