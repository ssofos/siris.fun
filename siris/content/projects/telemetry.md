---
title: "Telemetry"
date: 2022-07-14T13:02:31-07:00
draft: false
comments: false
images:
categories:
  - Projects
tags:
  - Telemetry
  - Prometheus
  - Grafana
---

A [Funtoo Project](https://www.funtoo.org/Special:CargoTables/projects) that I started in 2022 is the Funtoo Telemetry Project.

It is dedicated to modernizing and upgrading all the core Prometheus and Grafana related packages in Funtoo Linux so that the latest real time metrics collection and analysis tools are available to both desktop and server users.

The project is multi-faceted and covers different areas ranging from distribution packaging, software development, and building the latest generation of observability infrastructure for Funtoo Linux.

Check out the official project page at: **[Funtoo Telemetry Project](https://www.funtoo.org/Funtoo:Telemetry)**

